package Recursion;

public class FibonacciCompare {
	
	public static void main(String args[]) {
    	StringBuffer chart2 = new StringBuffer();
    	
		for(int i=1;i<=54;i++) {
	    	long start = java.lang.System.nanoTime();
			long f=FibIter(i);
	    	long end = java.lang.System.nanoTime();
	    	long iTime = end-start;
	    	

	    	start = java.lang.System.nanoTime();
			long f2=FibR(i);
	    	end = java.lang.System.nanoTime();
	    	long rTime = end-start;
	    	if(f!=f2) {
	    		System.out.println("Error!");
	    		System.exit(1);
	    	}
	    	double ditime=iTime*1.0/1000;
	    	double drtime=rTime*1.0/1000;
	    	String it = String.format("%.1f",ditime);
	    	String rt = String.format("%.1f",drtime);
			System.out.println(i+"&"+it+"&"+rt+"\\\\");
			//System.out.println(i+"&"+iTime+"&"+rTime+"\\\\");
			//System.out.println(i+" "+f2+" "+rTime);
			chart2.append(i+"&"+f2+"&"+rt+"&"+String.format("%.2f",(1.0*f2)/drtime)+"\\\\\n");
		}
		System.out.println("-----------");
		System.out.println(chart2);
	}
	public static long FibIter(int n) {
	    if (n <= 1) return(n);
	    else {
	        long fib=0, fibm1=1, fibm2=0, index=1;
	        while (index < n) {
	            fib = fibm1 + fibm2;
	            fibm2 = fibm1;
	            fibm1 = fib;
	            index = index + 1;
	        }
	        return(fib); 
	    }}
    public static long FibR(int n) {
        if (n <= 1) 
           return(n);
        else 
           return FibR(n-1) + FibR(n-2);
        }
    
}
