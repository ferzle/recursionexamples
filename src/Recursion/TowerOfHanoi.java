package Recursion;
import java.util.Stack;
/**
 * A very simple demonstration of the recursive algorithm to solve the Towers of Hanoi
 * 
 * @author Chuck Cusack, March 2013.
 *
 */
public class TowerOfHanoi {

	public static void main(String[] args) {
		TowerOfHanoi tow = new TowerOfHanoi(7);
		tow.solveTowers();
	}

	private int					numberDiscs;
	private final static int	NUMBER_PEGS	= 3;
	private Stack<Integer>[]	towers;

	public TowerOfHanoi(int numberDiscs) {
		super();
		this.numberDiscs = numberDiscs;
		towers = (Stack<Integer>[]) (new Stack[NUMBER_PEGS]);
		for (int i = 0; i < NUMBER_PEGS; i++) {
			towers[i] = new Stack<Integer>();
		}
		reset();
		drawTowers();
	}

	public void reset() {
		for(int i=0;i<NUMBER_PEGS;i++) {
			towers[i].clear();
		}	
		for (int i = numberDiscs; i > 0; i--) {
			towers[0].push(i);
		}
	}

	public void solveTowers() {
		reset();
		solveTowers(numberDiscs, 0, 1, 2);
	}

	private void solveTowers(int n, int source, int destination, int spare) {
		if (n == 1) {
			moveDisc(source, destination);
			drawTowers();
		} else {
			solveTowers(n - 1, source, spare, destination);
			solveTowers(1, source, destination, spare);
			solveTowers(n - 1, spare, destination, source);
		}
	}

	private boolean isValidPeg(int i) {
		return i >= 0 && i < NUMBER_PEGS;
	}

	public boolean moveDisc(int i, int j) {
		if (isValidPeg(i) && isValidPeg(j) && !towers[i].empty()
				&& (towers[j].empty() || towers[i].peek() < towers[j].peek())) {
			towers[j].push(towers[i].pop());
			return true;
		}
		return false;
	}

	public void drawTowers() {
		for(int i=numberDiscs;i>0;i--) {
			for(int j=0;j<NUMBER_PEGS;j++) {
				if(towers[j].size()>=i) {
					System.out.printf("%5d", towers[j].get(i-1));
				} else {
					System.out.printf("%5s", "|");
				}
			}
			System.out.println("");
		}
		System.out.println("-------------------\n\n");
	}
}
