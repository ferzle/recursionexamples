package Recursion;

public class SimpleRecursiveAlgorithms {

	public static int factorialIterative(int n) {
		int ans = 1;
		for (int i = n; i > 1; i--) {
			ans = ans * i;
		}
		return ans;
	}

	public static int factorialRecursive(int n) {
		if (n == 1) {
			return 1;
		} else {
			return n * factorialRecursive(n - 1);
		}
	}

	public static int fibRecursive(int n) {
		if (n == 1 || n == 2) {
			return 1;
		} else {
			return fibRecursive(n - 1) + fibRecursive(n - 2);
		}
	}

	public static int fibIterative(int n) {
		int FminusOne = 0;
		int FminusTwo = 0;
		int F = 1;
		for (int i = 1; i < n; i++) {
			FminusTwo = FminusOne;
			FminusOne = F;
			F = FminusOne + FminusTwo;
		}
		return F;
	}

	// ------------------------------------------------------------------------------
	// The following versions were added recently and are not part of the applet
	// yet.
	int[] fib;

	// ------------------------------------------------------------------------------
	// Dynamic Programming using memoization (or a top-down recursive approach)
	//
	public int fibDynamicTopDown(int n) {
		if (n < 2) {
			return n;
		}
		fib = new int[n + 1];
		fib[0] = 0;
		fib[1] = 1;
		for (int i = 2; i <= n; i++) {
			fib[i] = -1;
		}
		return fibDynamicHelper(n);
	}

	public int fibDynamicHelper(int n) {
		if (fib[n] < 0) {
			fib[n] = fibDynamicHelper(n - 1) + fibDynamicHelper(n - 2);
		}
		return fib[n];
	}

	// ------------------------------------------------------------------------------
	// Dynamic Programming using bottom-up approach (and removing recursion)
	//
	public int fibDynamicBottomUp(int n) {
		if (n < 2) {
			return n;
		}
		fib = new int[n + 1];
		fib[0] = 0;
		fib[1] = 1;
		for (int i = 2; i <= n; i++) {
			fib[i] = fib[i - 1] + fib[i - 2];
		}
		return fib[n];
	}
	// ------------------------------------------------------------------------------
}
