package Recursion;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class RecursionDemoApp extends JFrame {

	private JTextField	fib2Out;
	private JTextField	fib1Out;
	private JTextField	fact2Out;
	private JTextField	fact1Out;
	private JTextField	fact1In;
	private JTextField	fib2In;
	private JTextField	fib1In;
	private JTextField	fact2In;

	public static void main(String[] args) {
		new RecursionDemoApp();
	}

	public RecursionDemoApp() {
		JLabel Title = new JLabel("Sample recursive and iterative methods");
		Box mainBox = Box.createVerticalBox();

		JPanel fact1Box = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JButton fact1Button = new JButton("n! (recursive)");
		fact1In = new JTextField(6);
		fact1Out = new JTextField(10);

		JPanel fact2Box = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JButton fact2Button = new JButton("n!  (iterative) ");
		fact2In = new JTextField(6);
		fact2Out = new JTextField(10);

		JPanel fib1Box = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JButton fibRecButton = new JButton("fib(n) (recursive)");
		fib1In = new JTextField(6);
		fib1Out = new JTextField(10);
		
		JPanel fib2Box = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JButton fibItButton = new JButton("fib(n)  (iterative) ");
		fib2In = new JTextField(6);
		fib2Out = new JTextField(10);

		fact1In.setColumns(4);
		fact1Box.add(fact1In);
		fact1Box.add(fact1Button);
		fact1Box.add(fact1Out);

		fact2In.setColumns(4);
		fact2Box.add(fact2In);
		fact2Box.add(fact2Button);
		fact2Box.add(fact2Out);

		fib1In.setColumns(4);
		fib1Box.add(fib1In);
		fib1Box.add(fibItButton);
		fib1Box.add(fib1Out);

		fib2In.setColumns(4);
		fib2Box.add(fib2In);
		fib2Box.add(fibRecButton);
		fib2Box.add(fib2Out);

		fibRecButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int n = Integer.parseInt(fib2In.getText());
				int fibn = SimpleRecursiveAlgorithms.fibRecursive(n);
				fib2Out.setText(fibn + "");
			}
		});
		fibItButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int n = Integer.parseInt(fib1In.getText());
				int fibn = SimpleRecursiveAlgorithms.fibIterative(n);
				fib1Out.setText(fibn + "");

			}
		});
		fact1Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int n = Integer.parseInt(fact1In.getText());
				int nFactorial = SimpleRecursiveAlgorithms.factorialRecursive(n);
				fact1Out.setText(nFactorial + "");
			}
		});
		fact2Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int n = Integer.parseInt(fact2In.getText());
				int nFactorial = SimpleRecursiveAlgorithms.factorialIterative(n);
				fact2Out.setText(nFactorial + "");
			}
		});
		mainBox.add(fact2Box);
		mainBox.add(fact1Box);
		mainBox.add(fib1Box);
		mainBox.add(fib2Box);
		getContentPane().add(mainBox, BorderLayout.CENTER);
		getContentPane().add(Title, BorderLayout.NORTH);

		pack();
		setSize(new Dimension(400, 200));
		setVisible(true);
	}
}
